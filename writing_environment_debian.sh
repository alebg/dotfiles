#!/usr/bin/env bash


sudo apt-get -y update

# pandoc
sudo apt-get install -y pandoc

# Full TeXLive, but lean (no docs or src code)
sudo apt-get install -y texlive-full --no-install-recommends

# editor
sudo apt-get install -y vim

