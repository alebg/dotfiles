This is a personal repository of Linux dotfiles, which should be put in the home directory ($HOME).

## Automatic Setup

The `setup*.sh` scripts will automatically **remove** (WARNING!) the original local configuration files vim and other apps, and create symlinks to the equivalent files in this repository.

`setup_bash.sh` is safe as it will just add text at the end of ~/.profile and ~/.bashrc.


## Manual Setup

Simply remove the original local config files, and then create (soft) symlinks to the equivalent files in this repository:
`rm -rf /path/to/local/original`
`ln -s /path/to/repo/original /path/to/local/link`
