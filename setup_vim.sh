#!/usr/bin/env bash

# WARNING: This will remove any .vimrc file and .vim directory, and will put soft symlinks to the file and directory in this repo



script_dir="$( dirname "$0" )"
builtin cd $script_dir
script_full_path=${PWD}


cat << EOF
WARNING: this will delete ${HOME}/.vimrc and the ${HOME}/.vim directory and replace them by the corresponding files at ${script_dir}/ (this script's directory).

Do you wish to continue? [ Y / n ]
EOF
read confirmation
[ "$confirmation" != "Y" ] && echo "Aborting." && exit 0


# Dangerous part! Do not debug with this uncommented
builtin rm -f "${HOME}/.vimrc" \
&& echo "Removed ${HOME}/.vimrc" \
&& ln -s "${script_full_path}/.vimrc" "$HOME/.vimrc" \
&& echo "Successfully symlinked to ${HOME}/.vimrc"

builtin rm -rf "${HOME}/.vim" \
&& echo "Removed ${HOME}/.vim/" \
&& ln -s "${script_full_path}/.vim" "$HOME/.vim" \
&& echo "Successfully symlinked to ${HOME}/.vim/"

