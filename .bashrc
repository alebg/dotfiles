# SETUP
# ============

# 1. ADD THE FOLLOWING TO $HOME/.profile : (create the file if it doesn't exist)
# # if running bash
# if [ -n "$BASH_VERSION" ]; then
#     # include .bashrc if it exists
#     if [ -f "$HOME/.bashrc" ]; then
# 	. "$HOME/.bashrc"
#     fi
# fi

# 2. ADD THE FOLLOWING TO $HOME/.bashrc : (create the file if it doesn't exist)
# NOTE: This is done automatically by setup_bash.sh
# ============
# SOURCE PERSONAL CONFIGURATION
# ============
# . $HOME/.dotfiles/.bashrc


# Disable XON/XOFF mechanism
# It freezes the terminal with C-S,
# IT IS FKN ANNOYING
stty -ixon

# BASICS
# ============

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac


# BASH HISTORY
# ============

# Don't put duplicate lines or lines starting with space in the history
# See bash(1) for more options
HISTCONTROL=ignoreboth

# Append to the history file, don't overwrite it
shopt -s histappend

# For setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=10000
HISTFILESIZE=20000


# BASH CONFIG
# ============

# Check the window size after each command and, if necessary, update the values of LINES and COLUMNS
shopt -s checkwinsize

# Set vi key bindings
#set -o vi  # Doesn't work very well :/

# Add 256 color support for the terminal if it uses an older xterm
if [ "$TERM" == xterm-16color ]; then
    export TERM=xterm-256color
elif [ "$TERM" == xterm ]; then
    export TERM=xterm-256color
elif [ "$TERM" == xterm-color ]; then
    export TERM=xterm-256color
fi
# If it doesn't work, try
# export TERM=gnome-256color


# Further configuration depending on ${OSTYPE}
if [ "${OSTYPE}" == linux-gnu ]; then
    . $HOME/.dotfiles/.bashrc_cont_linux
elif [ "${OSTYPE}" == linux-android ]; then  # Works with Termux
    . $HOME/.dotfiles/.bashrc_cont_android
fi


# CUSTOM ALIASES AND SMALL FUNCTIONS
# ============
. $HOME/.dotfiles/.bash_aliases

# wherefunc FUNCTION:
#   locates the file and number line of a bash FUNCTION
#   as they do not appear e.g. under "alias"
wherefunc () { (shopt -s extdebug; declare -F "${*}"); }

# CUSTOM INPUTRC (FOR BASH ONLY)
# ============
. $HOME/.dotfiles/.bash_inputrc

# CUSTOM PROGRAMS
# ============
# Custom programs in ~/.bin are better than complex functions/commands
# They should be symlinked at ~/.bin, so add it to PATH if it exists
# But this adding to PATH must be done at .dotfiles/.profile
# To reload this, restart the computer or source ~/.profile

# These have a 'cd' inside and it doesn't work unless we do this
if [ -d $HOME/.bin ]; then
    [ -s $HOME/.bin/cribir_base ] && \. $HOME/.bin/cribir_base
    [ -s $HOME/.bin/cd_fzf ] && \. $HOME/.bin/cd_fzf
    [ -s $HOME/.bin/abrir_fzf ] && \. $HOME/.bin/abrir_fzf
fi


# TERMINAL APPS CONFIG
# ============

# Tilix Terminal: recommended fix by the app itself
if [ $TILIX_ID ] || [ $VTE_VERSION ]; then
    source /etc/profile.d/vte-2.91.sh
fi

