# Should be sourced in ~/.dotfiles/.bashrc


# ============
# COMMAND ALIASES AND SMALL FUNCTIONS
# ============

# Safe copy, move, and remove
alias cp='cp -i'
alias mv='mv -i'
alias rm='rm -iv'

# cd aliases
alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'
alias .....='cd ../../../..'
cdl() { cd $1; ls;}  # cd and ls

# ls aliases
# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# some more ls aliases
alias l='ls'
alias ll='ls -alFh'  # all, details, human-friendly file/directory size output
alias lc='ls -AlFh | cut -c22-$((21 + $COLUMNS))'  # all, details, human-friendly file/directory size output; truncate to commonly useful columns
alias la='ls -A'  # all
alias lgp='ls | grep'
alias lagp='ls -a | grep'

# Search command line history
alias hgrep="history | grep "

# Show the latest 10 files in a directory; pass a number to show more/less
recent() { ls -Artlh | tail -${1-10};}

# Show current directory and subdirectories
# max-depth = 1 by default, change by passing a number to the function
duh() { du -h --max-depth=${1-1} | sort -h 2> /dev/null;}

# Aliases to quickly show tree with different hierarchy levels
alias tree1='tree -L 1'
alias tree2='tree -L 2'
alias tree3='tree -L 3'
alias tree4='tree -L 4'

# Add an "alert" alias for long running commands
# It pops a notification when the command is over
    # USE: sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

# Find the 10 heaviest directories in disk at .; pass "-n" to show more/less
#alias diskusage='du -S | sort -nr | more'
alias directorysize='du -h | sort -nr | head'

# Fix back the terminal if printing a binary (or something else) messes the terminal behavior
# This uses many methods, at least one has to fix your problem!
alias fixterminal='reset; stty sane; tput rs1; clear; echo -e "\033c"'



# ============
# APPS ALIASES AND FUNCTIONS
# ============

# vim
alias vi='vim'
alias vmi='vim'  # common mistake I make

# git
alias grem='printf "======\nRemote status:\n======\n" && git remote -v update && printf "\n======\nLocal status:\n======\n" && git status'

# apt-cache + fzf: to look for and install apt packages; VERY nice!
alias aptf="apt-cache search '' | sort | cut --delimiter ' ' --fields 1 | fzf --multi --cycle --reverse --preview 'apt-cache show {1}' --preview-window=bottom,60% | xargs -r sudo apt install -y"

# abrir + fzf: NOTE: REQUIRES THE abrir SCRIPT ( wrapper on xdg-open)
# Use fzf across the home folder, ignoring hidden files at the top of the
# home folder; pass them to abrir
af() {
    find $HOME -not \( -path "$HOME/.*" -prune -false -o -wholename "$HOME/.*" -o -path "$HOME/R/x86_64-pc-linux-gnu-library" -prune -false -o -path "$HOME/list" -prune -false -o -wholename "$HOME/*.git*" -o -wholename "$HOME/*node_modules*" -o -wholename "$HOME/*virtualenv*" -o -wholename "$HOME/*venv*" \) -type f | fzf -m --reverse --cycle --preview='echo ""; echo "basename: $( basename {} )"; echo "default application to open: $( xdg-mime query filetype {} | xargs xdg-mime query default | sed -e "s/\..*$//" )"' --preview-window=bottom,4 | xargs -ro -d "\n" abrir
}

