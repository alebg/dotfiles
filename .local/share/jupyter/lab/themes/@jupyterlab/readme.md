
With this instructions we will modify the standard dark theme of jupyter to have more warm colors (gruvbox-like).

1. Create a backup of `@jupyterlab/themes-dark-extension`
2. Copy the two `index.*` files here into the `@jupyterlab/themes-dark-extension` folder
3. Enter jupyter lab and choose the `Dark Theme`

