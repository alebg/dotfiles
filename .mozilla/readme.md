
This folder contains a custom `chrome/userChrome.css` for a FireFox profile that will mimic Google Chrome's "app mode".
This is done by hiding the tab bar and the toolbar.

The original instructions found at the web (see the `.html` file in this directory) generated a FF window with an address bar, so I modified them.

## Instructions

1. First, at `about:profiles` open the root directory of the profile to be modified and get the path (e.g. `~/.mozilla/firefox/<somelongname>/`)

2. Symlink the folder `(...)/chrome/` inside this directory to `chrome/` inside the profile's root directory
E.g. `ln -s ~/.dotfiles/.mozilla/firefox/ale-app-prof-root/chrome ~/.mozilla/firefox/e6oc4t96.jupyterApp/chrome`

3. Start FireFox using the profile (e.g. `firefox -P app`) and at the `about:config`, using the FireFox profile, set `toolkit.legacyUserProfileCustomizations.stylesheets` to `true`

4. Close FireFox and open it again using the profile to see the changes.
