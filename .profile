# SETUP
# ============
# ~/.profile is read by most shells at the first session login after startup
#   i.e., reloading the graphical X login will NOT reload .profile
#   to reload it, source ~/.profile or reboot the computer
#   for bash, if ~/.bash_profile or ~/.bash_login exist, ~/.profile will NOT be read
# If .profile is not being loaded, check your terminal app (e.g., tilix),
#   you may need to configure the option "start as login shell"

# 1. ADD THIS AT THE END OF $HOME/.profile (after sourcing .bashrc; see .bashrc)
# ============
# SOURCE PERSONAL PROFILE
# ============
# . $HOME/.dotfiles/.bash_profile


# PERSONAL CONFIGURATION
# ============

# Further configuration depending on ${OSTYPE}
if [ "${OSTYPE}" == linux-gnu ]; then
    . $HOME/.dotfiles/.profile_cont_linux
elif [ "${OSTYPE}" == linux-android ]; then
    . $HOME/.dotfiles/.profile_cont_android
fi

# CUSTOM PROGRAMS
# ============

# They should be at ~/.bin, so add it to PATH if it exists
# Added in the end so custom scripts with the same name as actual programs don't take precedence
if [ -d "$HOME/.bin" ] ; then
    PATH="$PATH:$HOME/.bin"
fi

# SSH KEYS
# ============

if [ -z "$SSH_AUTH_SOCK" ]; then
   # Check how many running instances of ssh-agent there are
   RUNNING_AGENT="`ps -ax | grep 'ssh-agent -s' | grep -v grep | wc -l | tr -d '[:space:]'`"
   if [ "$RUNNING_AGENT" = "0" ]; then
        # Launch a new instance of the agent
        ssh-agent -s &> $HOME/.ssh/ssh-agent
   fi
   eval `cat $HOME/.ssh/ssh-agent`
fi
