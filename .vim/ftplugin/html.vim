" HTML filetype specific configuration
" Note: May override global .vimrc configuration
" Use `setlocal` instead of `set`!

" Reset configuration
nnoremap <Leader>v :source ~/.vimrc<CR>:source ~/.vim/ftplugin/html.vim<CR>:nohl<CR>:redraw!<CR>


"""
" SPELLING AND DICTIONARIES:
"""
setlocal spell                    " Set dictionary
setlocal spelllang=en,es,it,fr    " Set languages


"""
" KEYBOARD SHORTCUTS:
"""
" Note: many use the custom vim command 'Silent'

" Annoying shit
inoremap <C-Space> <Nop>

" Open the file as it is in FF, 'app' profile (no plugins, no distractions)
map <Leader>f :w!<CR>:Silent !firefox -P app "%" > /dev/null 2> /dev/null&<CR><CR>

" Save the file as a standalone html (works very fast)
map <Leader>h :w! "%:p"<CR>:!pandoc -s --pdf-engine=xelatex -o "%:p:h"/"%:r"-preview.html "%:p" > /dev/null 2> /dev/null &<CR><CR>
" Open the generated html file in FF (needs 'app' profile)
map <Leader>oh :Silent !firefox -P app "%:p:h"/"%:r"-preview.html > /dev/null 2> /dev/null&<CR><CR>

" Auto-complete opening parentheses
"inoremap ( ()<Left>
"inoremap [ []<Left>

"""
" TAG AUTOCOMPLETION:
"""

" Insert image tags

" Emulate Ctrl + b/i for bold/italics of selections
" NOTE: Ctrl + i is THE SAME as <TAB> for terminals!!

