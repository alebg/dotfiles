" Markdown filetype specific configuration
" Note: May override global .vimrc configuration
" Use `setlocal` instead of `set`!

" DEPENDENCIES:
" Personal scripts at https://gitlab.com/AleBG/dotbin
" put somewhere in PATH with chmod +x
"   panmd-to-pdf
"   execyaml-compile
"   execyaml
"   readyaml

" Reload config file
nnoremap <Leader>v :source ~/.vimrc<CR>:source ~/.vim/ftplugin/markdown.vim<CR>:nohl<CR>:redraw!<CR>

" Source custom functions for markdown
source ~/.vim/vimrc_cont/custom_functions_md.vim

"""
" SPELLING AND DICTIONARIES:
"""
setlocal spell  " Set dictionary
setlocal spelllang=es,en,it,fr,de  " Set languages


"""
" BOILERPLATE:
"""
" yaml block for pandoc
nnoremap <Leader>b ggO<CR><CR><Esc>gg:r ~/.vim/boilerplate/markdown<CR>ggdd
" Image boilerplate code
map <Leader>i i![]<Right>(img/)<C-O>${ width=100% <C-O>0<Esc>f/
" Personal note
nnoremap <Leader>n A<Space>\<CR>`>><Space>`\<Left><Left>


"""
" COMPILATION AND EXECUTION SHORTCUTS:
"""
" Note: many use the custom vim command 'Silent'

" PDF: Compile the file using the custom shell program 'panmd-to-pdf'
  " uses the yaml file passed at the 'yaml-format' key of the document
  " depends on 'panmd-to-pdf' and 'readyaml'!
nnoremap <F5> :w!<CR>:Silent !panmd-to-pdf "%:p"<CR>
inoremap <F5> <Esc>:w!<CR>:Silent !panmd-to-pdf "%:p"<CR>
" Executes the command at 'compile-shell-command' of the file's yaml block
" Uses the custom shell program 'execyaml-compile'
nnoremap <F6> :Silent !execyaml-compile "%" &<CR>
inoremap <F6> <Esc>:Silent !execyaml-compile "%" &<CR>
" Open: the corresponding pdf with the custom shell program 'abrir'
"   ignores the initial "." if the file has one
map <Leader>op :Silent ! file="%:t" ; if [ "${file:0:1}" = "." ] ; then filep=${file:1} ; abrir "${filep\%.md}.pdf" ; else abrir "${file\%.md}.pdf" ; fi<CR>

" HTML: Compile the file as a standalone html (works very fast)
nnoremap <F7> :w! "%:p"<CR>:!pandoc -s --pdf-engine=xelatex -o "%:p:h"/"%:r"-preview.html "%:p" > /dev/null 2> /dev/null &<CR><CR>
" Open: the generated html file in a browser
map <Leader>oh :!abrir "%:p:h"/"%:r"-preview.html > /dev/null 2> /dev/null&<CR><CR>

" Execute: any of the YAML commands of the file's yaml block
" (the key needs to be passed inside the empty quotemarks)
nnoremap <F8> :Silent !execyaml "%" "" &<Left><Left><Left>
inoremap <F8> <Esc>:Silent !execyaml "%" "" &<Left><Left><Left>


"""
" KEYBOARD SHORTCUTS:
"""

" Emulate Ctrl + b/i for bold/italics of selections
" NOTE: Ctrl + i is THE SAME as <TAB> for terminals!!
xnoremap <C-b> <Esc>`>a*<Esc>`<i*<Esc><Right>vt*
inoremap <C-b> **<Left>

" Surround for HTML comments (works in Markdown)
xnoremap sc <Esc>`>a --><Esc>`<i<!-- <Esc>
"nnoremap <Leader>c <Esc>a<!-- --><Esc>4ha<Space>

" Use a custom function to dump text sequences of the form x₀, x₁, …, xₙ
" DEPENDENCY: the function 'Sequence', at
" ~/.vim/vimrc_cont/custom_functions_md.vim
inoremap <C-S> <C-R>=Sequence('')<Left><Left>

" Auto-completion
inoremap { {}<Left>

"""
" PANDOC: Markdown + TeX + YAML syntax fix
"""
" This is necessary, otherwise Markdown syntax highlights are messed up when
" using TeX or YAML syntax
"
" gruvbox syntax colors: Type (yellow); Macro (acqua); Comment (gray);
" Delimiter (red)

" TEX SYNTAX:
" Inline math $...$ and math blocks $$... \n ...$$
syntax region TeXMath oneline matchgroup=Delimiter start="\$" end="\$"
syntax region TeXMath matchgroup=Delimiter start=/\$\$/ end=/\$\$/ contains=TeXMath
" Environments
syntax region TeXEnv matchgroup=Macro start=/begin{.*}/ end=/end{.*}/
" Ignore spelling in TeXEnv
syntax cluster TeXEnv add=@NoSpell
" Commands; end at end of word ('\>'; and '$' is end of line)
syntax match TeXComm "\\"
" Command Parameter
syntax region TeXParam matchgroup=Macro start="{" end="}"
" Comments, start with '[\\] : #', end at the end of the line ('$' in vim)
"syntax region TeXCmnt matchgroup=Macro start="\[\\\\\]: #" end="$"  " Better
"use HTML comments
" _text_ is bold in markdown, but the default highlight interferes when
" writing LaTeX formulas, which is very annoying, so I disabled it
"syntax region MdBoldUnderLine matchgroup=Macro start="_" end="\>"

" Link the custom formats with predefined vim formats highlights
highlight link TeXMath Normal
highlight link TeXEnv Normal
highlight link TeXComm Type
highlight link TeXParam Type
"highlight link TeXCmnt Comment
"highlight link MdBoldUnderLine Normal


" YAML SYNTAX:
" YAML blocks inside files
syntax region YAMLblock matchgroup=Delimiter start="\-\-\-$" end="\.\.\.$\|\-\-\-$" contains=YAMLblock
" The \ are escape characters, and the $ mark the end of the delimiter,
" otherwise markdown tables are interpreted as YAML blocks

" Link the custom formats with predefined vim formats highlights
highlight link YAMLblock Type


"""
" SEMIPLUGIN:
" 'mdtools'
"""
" Bits stolen and adapted from 'plasticboy/vim-markdown' to:
"   - Generate a navigable TOC in the location list window
"   - Automatically beautify markdown tables
" At '$HOME/.vim/autoload/mdtools.vim'

" Call TOC window
command! -buffer Toc call mdtools#Toc('horizontal')
command! -buffer Toch call mdtools#Toc('horizontal')
command! -buffer Tocv call mdtools#Toc('vertical')
command! -buffer Toct call mdtools#Toc('tab')
nnoremap <Leader>to :Toc<CR>
nnoremap <Leader>tc :lclose<CR>

" Shrink Toc location list when possible
let g:vim_markdown_toc_autofit = 1

" Call TableFormat
" IMPORTANT: depends on the 'godlygeek/tabular' plugin
command! -buffer TableFormat call mdtools#TableFormat()
command! -buffer FormatTable call mdtools#TableFormat()

