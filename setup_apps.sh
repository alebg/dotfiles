#!/usr/bin/env bash

# WARNING: This will remove several dotfiles and directories, and will put soft symlinks to the equivalent files and directories in this repo



script_dir="$( dirname "$0" )"
builtin cd $script_dir
script_dir=${PWD##*/}


cat << EOF
Pass any number of files' or directories' full names as starting from ${PWD} to replace the original versions at the by symlinks to .

EOF

function usage() {
cat << EOF
Pass only these particular files, since the directories are not complete:

.config/Code/User/keybindings.json
.config/Code/User/settings.json
.config/redshift/redshift.conf
.config/texstudio/original_theme
.emacs.d/init.el
.jupyter/jupyter_notebook_config.py

The same for these particular directories:

.config/Abricotine/app/themes/warm
.config/geany/colorschemes

Passing these entire directories is okay:
.cmus
.nanorc

EOF
}


case "${1}" in
    "-h" | "--help")
        usage
        exit 0 ;;
esac


for file in "${@}" ; do
    repo_original="${HOME}/${script_dir}/${file}"
    local_target="${HOME}/${file}"

    case "${file}" in
        *vim*)
            printf "\nPlease use ./setup_vim.sh to correctly manage ${file}\n"
            continue ;;

        *bash*)
            printf "\nPlease use ./setup_bash.sh to correctly manage ${file}\n"
            continue ;;
    esac


    if [ -f "${repo_original}" ] || [ -d "${repo_original}" ] ; then
        echo ""
        echo "Please confirm for $file [ Y / n ]"
        read confirmation
        [ "${confirmation}" != "Y" ] && printf "$file skipped.\n" && continue

        rm -rf "${local_target}"
        ln -s "${repo_original}" "${local_target}"
        # Debug
        #echo "rm -rf \"${local_target}\""
        #echo "ln -s \"${repo_original}\" \"${local_target}\""

        [ $? -ne 0 ] && printf "ln failed for ${file}\n Please fix the error and try again later.\n" && continue

        printf "Successfully soft-symlinked ${repo_original} -> ${local_target}\n"

    else
        printf "${repo_original} doesn't exist! Moving to next file.\n"
    fi

done

