" See :digraphs for the possible combinations. E.g (many work also backwards)
" NO ¬    AN ∧    OR ∨    FA ∀    TE ∃    -T ⊥
" <- ←    -> →    <> ↔    <= ⇐    => ⇒    == ⇔
" .: ∴    :. ∵    :R ∶    :: ∷    ., …    :3 ⋮    .3 ⋯
" /0 ∅    (- ∈    (_ ⊆    )_ ⊇    (U ∩    )U ∪    (C ⊂    )C ⊃    -) ∋
" +- ±    :- ÷    =< ≤    >= ≥    != ≠
" 00 ∞    +Z ∑    *P ∏    1' ′    2' ″    3' ‴
" .P ⋅    2! ‖    dP ∂    DE ∆    NB ∇    In ∫    DI ∬    Io ∮
" << «    >> »    SE §    -M —    -N –
" =3 ≡    ?2 ≈    ?- ≃    ?= ≅    =? ≌
" <letter>* <greek>       E.g. f* φ   F*  Φ
" <number>S <superscript> E.g. 2S ²
" <number>s <subscript>   E.g. 2s ₂

" Turns on for insert mode: `char1<BS>char2` to produce a digraph
  " Works with the same combinations as <C-K> in insert mode
"set digraph

" Custom shortcuts for other, not included in vim, UNICODE diagraphs
" digraph xy NNNN   will insert the UNICODE symbol NNNN with C-Kxy
" Use ga in normal mode above a character to see its UNICODE code

digraph dp 8706  " ∂ using C-Kdp, not only dP which is confusing
digraph ~= 8776  " ≈ using C-K~=, not only ?2 which is confusing
digraph nb 8711  " ∇, nabla
digraph gr 8711  " ∇, nabla/gradient
digraph in 8747  " ∫, integral
digraph di 8748  " ∬, double integral
digraph io 8748  " ∮, contour integral
digraph rs 8638  " ↾, set restriction
digraph ba 8896  " ⋀, big AND
digraph BA 8896  " ⋀, big AND
digraph Ba 8896  " ⋀, big AND
digraph bA 8896  " ⋀, big AND
digraph bo 8897  " ⋁, big OR
digraph BO 8897  " ⋁, big OR
digraph Bo 8897  " ⋁, big OR
digraph bO 8897  " ⋁, big OR

digraph nS 8319  " ⁿ
digraph Sn 8319  " ⁿ
digraph mS 7504  " ᵐ
digraph Sm 7504  " ᵐ
digraph aS 7491  " ᵃ
digraph Sa 7491  " ᵃ
digraph bS 7495  " ᵇ
digraph SB 7495  " ᵇ
digraph iS 8305  " ⁱ
digraph Si 8305  " ⁱ
digraph jS 690   " ʲ
digraph Sj 690   " ʲ
digraph xS 739   " ˣ
digraph Sx 739   " ˣ
digraph yS 696   " ʸ
digraph Sy 696   " ʸ
digraph zS 7611  " ᶻ
digraph Sz 7611  " ᶻ
digraph as 8336  " ₐ
digraph sa 8336  " ₐ
digraph is 7522  " ᵢ
digraph si 7522  " ᵢ
digraph js 11388  " ⱼ
digraph sj 11388  " ⱼ
digraph ks 8342  " ₖ
digraph sk 8342  " ₖ
digraph ns 8345  " ₙ
digraph sn 8345  " ₙ
digraph ms 8344  " ₘ
digraph sm 8344  " ₘ


inoremap <C-K>.\ ⋱
inoremap <C-K>\. ⋱
inoremap <C-K>./ ⋰
inoremap <C-K>/. ⋰
inoremap ,lan ∧
digraph an 8743  " ∧
digraph aN 8743  " ∧
digraph An 8743  " ∧
digraph na 8743  " ∧
digraph Na 8743  " ∧
digraph nA 8743  " ∧
inoremap ,blan ⋀
inoremap ,lor ∨
digraph or 8744  " ∨
digraph oR 8744  " ∨
digraph Or 8744  " ∨
digraph ro 8744  " ∨
digraph Ro 8744  " ∨
digraph rO 8744  " ∨
inoremap ,blor ⋁
inoremap ,lno ¬
inoremap ,lra →
inoremap ,nec □
inoremap ,prob ◊
inoremap ,proof ■
inoremap ,cd ⊥
inoremap ,contrad ⊥
inoremap ,taut ⊤
inoremap ,tt ⊤
" Syntactically implies
inoremap ,sti ⊢
inoremap ,nsti ⊬
" Semantically implies
inoremap ,smi ⊨
inoremap ,nsmi ⊭
inoremap ,notin ∉<Space>
inoremap ,ni ∉<Space>
inoremap ,in ∈<Space>
inoremap ,ss ⊆
inoremap ,subs ⊆
inoremap ,es ∅
inoremap ,emptys ∅
inoremap ,fa ∀
inoremap ,ex ∃
inoremap ,te ∃
inoremap ,cup ∪
inoremap ,cap ∩
inoremap ,bcup ⋃
inoremap ,bcap ⋂
inoremap ,def ≔
inoremap ,neq ≠
inoremap ,leq ≤
inoremap ,geq ≥
inoremap ,ord ≺
inoremap ,sum ∑
inoremap ,bsum ∑\limits
inoremap ,prod ∏
inoremap ,bprod ∏\limits
inoremap ,< ⟨
inoremap ,> ⟩
inoremap ,o+ ⊕
inoremap ,mt ↦
inoremap ,mp ↦

inoremap ,ps 𝒫
inoremap ,wsp ℘
inoremap ,aleph ℵ
inoremap ,fkc ℭ
inoremap ,po 𝓟

inoremap ,bba 𝔸
inoremap ,bbb 𝔹
inoremap ,bbc ℂ
inoremap ,bbd 𝔻
inoremap ,bbe 𝔼
inoremap ,bbf 𝔽
inoremap ,bbg 𝔾
inoremap ,bbh ℍ
inoremap ,bbi 𝕀
inoremap ,bbj 𝕁
inoremap ,bbk 𝕂
inoremap ,bbl 𝕃
inoremap ,bbm 𝕄
inoremap ,bbn ℕ
inoremap ,bbo 𝕆
inoremap ,bbp ℙ
inoremap ,bbq ℚ
inoremap ,bbr ℝ
inoremap ,bbs 𝕊
inoremap ,bbt 𝕋
inoremap ,bbu 𝕌
inoremap ,bbv 𝕍
inoremap ,bbw 𝕎
inoremap ,bbx 𝕏
inoremap ,bby 𝕐
inoremap ,bbz ℤ

inoremap ,cca 𝒜
inoremap ,ccb ℬ
inoremap ,ccc 𝒞
inoremap ,ccd 𝒟
inoremap ,cce ℰ
inoremap ,ccf ℱ
inoremap ,ccg 𝒢
inoremap ,cch ℋ
inoremap ,cci ℐ
inoremap ,ccj 𝒥
inoremap ,cck 𝒦
inoremap ,ccl ℒ
inoremap ,ccm ℳ
inoremap ,ccn 𝒩
inoremap ,cco 𝒪
inoremap ,ccp 𝒫
inoremap ,ccq 𝒬
inoremap ,ccr ℛ
inoremap ,ccs 𝒮
inoremap ,cct 𝒯
inoremap ,ccu 𝒰
inoremap ,ccv 𝒱
inoremap ,ccw 𝒲
inoremap ,ccx 𝒳
inoremap ,ccy 𝒴
inoremap ,ccz 𝒵

inoremap ,bfA 𝐀
inoremap ,bfB 𝐁
inoremap ,bfC 𝐂
inoremap ,bfD 𝐃
inoremap ,bfE 𝐄
inoremap ,bfF 𝐅
inoremap ,bfG 𝐆
inoremap ,bfH 𝐇
inoremap ,bfI 𝐈
inoremap ,bfJ 𝐉
inoremap ,bfK 𝐊
inoremap ,bfL 𝐋
inoremap ,bfM 𝐌
inoremap ,bfN 𝐍
inoremap ,bfO 𝐎
inoremap ,bfP 𝐏
inoremap ,bfQ 𝐐
inoremap ,bfR 𝐑
inoremap ,bfS 𝐒
inoremap ,bfT 𝐓
inoremap ,bfU 𝐔
inoremap ,bfV 𝐕
inoremap ,bfW 𝐖
inoremap ,bfX 𝐗
inoremap ,bfY 𝐘
inoremap ,bfZ 𝐙

inoremap ,bfa 𝐚
inoremap ,bfb 𝐛
inoremap ,bfc 𝐜
inoremap ,bfd 𝐝
inoremap ,bfe 𝐞
inoremap ,bff 𝐟
inoremap ,bfg 𝐠
inoremap ,bfh 𝐡
inoremap ,bfi 𝐢
inoremap ,bfj 𝐣
inoremap ,bfk 𝐤
inoremap ,bfl 𝐥
inoremap ,bfm 𝐦
inoremap ,bfn 𝐧
inoremap ,bfo 𝐨
inoremap ,bfp 𝐩
inoremap ,bfq 𝐪
inoremap ,bfr 𝐫
inoremap ,bfs 𝐬
inoremap ,bft 𝐭
inoremap ,bfu 𝐮
inoremap ,bfv 𝐯
inoremap ,bfw 𝐰
inoremap ,bfx 𝐱
inoremap ,bfy 𝐲
inoremap ,bfz 𝐳

