" ############
" DEFAULTS
" ############

set confirm       " Ask for confirmation instead of failing commands
set clipboard+=unnamedplus  " Vim's clipboard same as the machine's
set ignorecase    " Case insensitive search
set mouse=a       " Enable use of the mouse for all modes
set noshowmode    " Don't show the name of modes (INSERT MODE, etc.)
set noswapfile    " Didn't have a good experience with .vimtmp and swap files
set number        " Line numbers
set scrolloff=12  " Keep N spaces between cursor and screen edges
set showmatch     " Highlight matching brackets
set smartcase     " except when using capital letters
set ttimeoutlen=0 " Prevent delay between normal and insert modes
set wildignorecase  " Ignore casing in autocomplete and wildmenu

" Add dictionary completion to the default insert mode completion (C-P; C-N)
" The dictionary is set automatically when spelling is set
set complete+=k


" ############
" INDENTATION
" ############

filetype indent on  " Set autoindent

" Indentation by filetype management
function! AleIndent() abort

  if &ft =~ 'vim\|html\|tex\|markdown'
    set shiftwidth=2
    set softtabstop=2
    set expandtab
    return
  endif

  if &ft =~ 'text\|python\|sh\|R'
    set shiftwidth=4
    set softtabstop=4
    set expandtab
    return
  endif
  " All other filetypes:
  set shiftwidth=4
  set softtabstop=4
  set expandtab
endfun

augroup ApplyAleIndent
  autocmd!
  autocmd BufEnter * call AleIndent()
augroup END


" ############
" PLUGINS
" ############

" Suggested by the vim-tex plugin github page:
filetype plugin indent on
syntax enable

" Config of `packer` plugin manager
lua require('plugins')

" See $VIMRUNTIME/plugin/vimtex.vim for the vimtex plugin configuration
" See $VIMRUNTIME/plugin/coc.vim for the coc plugin configuration


" ############
"  DIGRAPHS AND SPECIAL SYMBOLS
" ############

source ~/.config/nvim/init_digraphs.vim


" ############
"  APPEARANCE
" ############

source ~/.config/nvim/init_appearance.vim


" ############
" KEYMAPPINGS
" ############

source ~/.config/nvim/init_keymappings.vim


" ############
" CUSTOM TEXT OBJECTS
" ############

" '[l]ine since first non-blank character' text object
xnoremap il g_o^
onoremap il :normal vil<CR>
xnoremap al $o^
onoremap al :normal val<CR>

" 'Full [L]ine' text object
xnoremap iL g_o0
onoremap iL :normal ViL<CR>
xnoremap aL $o0
onoremap aL :normal VaL<CR>


" ############
" CUSTOM COMMANDS
" ############

" Silence external commands and avoid the need of redrawing the screen
command! -nargs=+ Silent execute 'silent <args>' | redraw! | nohl

