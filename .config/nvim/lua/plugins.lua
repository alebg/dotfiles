-- This file can be loaded by calling `lua require('plugins')` from your init.vim

return require('packer').startup(function(use)

  -- Packer can manage itself
  use 'wbthomason/packer.nvim'

  -- Simple plugins can be specified as strings
  use 'lervag/vimtex'

  -- Extension to use nodejs and language servers (coc-vimtex, coc-ltex)
  use {'neoclide/coc.nvim', branch = 'release'}

  -- GH Copilot
  use 'github/copilot.vim'

end)
