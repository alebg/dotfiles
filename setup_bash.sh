#!/usr/bin/env bash

# WARNING: This will modify your ~/.bashrc and ~/.profile
# Do this only once!

# Grab this script's directory by forcing cd into it
# should be the same as the personal .profile and .bashrc files
script_dir="$( dirname "$0" )"
builtin cd $script_dir
script_full_path=${PWD}


cat << EOF >> $HOME/.profile

# ============
# SOURCE PERSONAL .profile (FOR LOGIN SESSIONS)
# ============

. ${script_full_path}/.profile

EOF
[ $? -eq 0  ] && echo "Successfully sourced to $HOME/.profile"


cat << EOF >> $HOME/.bashrc

# ============
# SOURCE PERSONAL .bashrc (FOR NON-LOGIN SESSIONS)
# ============

. ${script_full_path}/.bashrc

EOF
[ $? -eq 0 ] && echo "Successfully sourced to $HOME/.bashrc"

